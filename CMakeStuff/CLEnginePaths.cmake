
# ogre
set(OGRE_SDK_ROOT "../OgreSDK-1.9.0")
set(OGRE_FRAMEWORK_DIR "${OGRE_SDK_ROOT}/lib/macosx/Release")

set(OGREOVERLAY_FRAMEWORK_HEADERS_FOR_MISCONFIGURED_OGREOVERLAY "${OGRE_FRAMEWORK_DIR}/OgreOverlay.framework/Headers")
set(OGREOVERLAY_RAW_HEADERS_FFS "${OGRE_SDK_ROOT}/include/OGRE/Overlay")

set(OGRE_FRAMEWORK_HEADERS_FOR_MISCONFIGURED_OGREOVERLAY "${OGRE_FRAMEWORK_DIR}/Ogre.framework/Headers")

# others
set(DISTANCE_FIELD_ROOT CLEngine/src/1stparty/InterpolatingDistanceField)
set(SPATIAL_HASH_ROOT CLEngine/src/1stparty/SpatialHash)

set(VEGAFEM_ROOT CLEngine/src/3rdparty/VegaFEM)
set(CARTWHEEL_ROOT CLEngine/src/3rdparty/ua-cartwheel)



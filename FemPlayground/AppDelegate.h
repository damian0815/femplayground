#ifndef __AppDelegate_H__
#define __AppDelegate_H__

#include "OgrePlatform.h"

#if OGRE_PLATFORM != OGRE_PLATFORM_APPLE
#error This header is for use with Mac OS X only
#endif

#ifdef __OBJC__

#import <Cocoa/Cocoa.h>
#import <QuartzCore/QuartzCore.h>
#import "FPApp.h"

// All this does is suppress some messages in the run log.  NSApplication does not
// implement buttonPressed and apps without a NIB have no target for the action.
@implementation NSApplication (_suppressUnimplementedActionWarning)
- (void) buttonPressed:(id)sender
{
    /* Do nothing */
}
@end


#if defined(MAC_OS_X_VERSION_10_6) && MAC_OS_X_VERSION_MAX_ALLOWED >= MAC_OS_X_VERSION_10_6
@interface AppDelegate : NSObject <NSApplicationDelegate>
#else
@interface AppDelegate : NSObject
#endif
{
	//CVDisplayLinkRef mDisplayLink;
    NSTimer *mTimer;
    FPApp mApp;
    
    NSDate *mDate;
    double mStartTime;
}

- (void)go;
- (void)renderOneFrame:(id)sender;

@property (retain) NSTimer *mTimer;
@property (nonatomic) double mStartTime;

@end

#if __LP64__
static id mAppDelegate;
#endif

@implementation AppDelegate

@synthesize mTimer;
@dynamic mStartTime;

- (void)go {
    
    mStartTime = 0;
	mTimer = nil;
    
    try {
		NSProcessInfo* processInfo = [NSProcessInfo processInfo];
		
		std::vector<std::string> arguments;
		for ( NSString* arg in [processInfo arguments] ) {
			arguments.push_back( [arg UTF8String] );
		}
        mApp.start( arguments );
        
        Ogre::Root::getSingleton().getRenderSystem()->_initRenderTargets();
        
        // Clear event times
		Ogre::Root::getSingleton().clearEventTimes();
    } catch( Ogre::Exception& e ) {
        std::cerr << "An exception has occurred: " <<
        e.getFullDescription().c_str() << std::endl;
    }
    
	// 60 fps
	//CVDisplayLinkCreateWithActiveCGDisplays( &mDisplayLink );
    mTimer = [NSTimer timerWithTimeInterval:(NSTimeInterval)(1.0f / 60.0f)
                                              target:self
                                            selector:@selector(renderOneFrame:)
                                            userInfo:nil
                                             repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:mTimer forMode:NSRunLoopCommonModes];
}


- (void)applicationDidFinishLaunching:(NSNotification *)application {
    mStartTime = 0;
    mTimer = nil;
    
    [self go];
}

- (void)renderOneFrame:(id)sender
{
    if(!OgreFramework::getSingletonPtr()->isOgreToBeShutDown() &&
       Ogre::Root::getSingletonPtr() && Ogre::Root::getSingleton().isInitialised())
    {
		if(OgreFramework::getSingletonPtr()->m_pRenderWnd->isActive())
		{
			double currentTime = OgreFramework::getSingletonPtr()->m_pTimer->getMilliseconds();
			double elapsed = currentTime-mStartTime;
			mStartTime = currentTime;
            
			
			OgreFramework::getSingletonPtr()->m_pKeyboard->capture();
			OgreFramework::getSingletonPtr()->m_pMouse->capture();
            
			OgreFramework::getSingletonPtr()->updateOgre(elapsed);
			
			double currentTimeSeconds = (double)currentTime/1000.0;
			mApp.frameTick( currentTimeSeconds );
			
			OgreFramework::getSingletonPtr()->m_pRoot->renderOneFrame();
		}
    }
    else
    {
        [mTimer invalidate];
        mTimer = nil;
        [NSApp performSelector:@selector(terminate:) withObject:nil afterDelay:0.0];
    }
}

- (void)dealloc {
    if(mTimer)
    {
        [mTimer invalidate];
        mTimer = nil;
    }
    
}

@end

#endif

#endif

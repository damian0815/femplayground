//
// Created by Damian Stewart on 03/05/2017.
//

#include "CLDeduplicatedMeshFactory.h"

#include "OgreMeshManager.h"
#include "CLGeometry.h"
#include "CLDeduplicatedMesh.h"

using std::set;

namespace CLDeduplicatedMeshFactory
{
    struct MeshReferenceDetails
    {
        Ogre::Entity* entity = nullptr;
        Ogre::SceneNode* sceneNode = nullptr;
        map<string,set<size_t> > vertexGroups;
        string vertexGroupName;
		string restBoneName;
    };

    static string ensureUniqueName(string startName, std::function<bool(string nameToCheck)> nameAlreadyExists)
    {
        string actualName = startName;
        int uniqueNameSuffix = 1;
        while(nameAlreadyExists(actualName)) {
            char buffer[4096];
            snprintf(buffer, 4096, "%s.%i", actualName.c_str(), uniqueNameSuffix);
            actualName = buffer;
            ++uniqueNameSuffix;
        }
        return actualName;
    }

    static Ogre::Entity* createUniqueMeshEntity(Ogre::SceneManager* sceneManager, string entityName, string meshFilename)
    {
        auto mm = Ogre::MeshManager::getSingletonPtr();
        auto existingMesh = mm->getByName(meshFilename);
        Ogre::MeshPtr mesh;
        if (existingMesh.isNull()) {
            mesh = mm->load(meshFilename, Ogre::ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME);
        } else {
            auto uniqueName = ensureUniqueName(meshFilename, [mm](string nameToCheck) { return !mm->getByName(nameToCheck).isNull(); });
            mesh = existingMesh->clone(uniqueName);
        }
        return sceneManager->createEntity(entityName, mesh);
    }


    static MeshReferenceDetails
    getMeshReferenceOrLoad(Ogre::SceneManager *sceneManager, const RTFConfigFile &definition, string definitionGroupLabel)
    {
        struct MeshReferenceDetails meshReferenceDetails;
        string entityName = definition.getStringSetting("entityName", definitionGroupLabel);
        string sceneNodeName = definition.getStringSetting("sceneNodeName", definitionGroupLabel);
        if (entityName.length()) {
            bool shared = definition.getBoolSetting("shared", definitionGroupLabel, false);
            if (shared) {
                meshReferenceDetails.entity = sceneManager->getEntity(entityName);
                meshReferenceDetails.sceneNode = sceneManager->getSceneNode(sceneNodeName);
            } else {
                auto meshFilename = definition.getStringSetting("mesh", definitionGroupLabel);
                auto actualEntityName = ensureUniqueName(entityName, [sceneManager](string nameToCheck) { return sceneManager->hasEntity(nameToCheck); });
                if (actualEntityName != entityName) {
                    DLog("while loading deduplicated mesh reference: renamed %s to %s", entityName.c_str(), actualEntityName.c_str());
                }
                meshReferenceDetails.entity = createUniqueMeshEntity(sceneManager, actualEntityName, meshFilename);

                auto actualSceneNodeName = ensureUniqueName(sceneNodeName, [sceneManager](string nameToCheck) { return sceneManager->hasSceneNode(nameToCheck); });
                if (actualSceneNodeName != sceneNodeName) {
                    DLog("while loading deduplicated mesh reference: renamed %s to %s", sceneNodeName.c_str(), actualSceneNodeName.c_str());
                }
                meshReferenceDetails.sceneNode = sceneManager->getRootSceneNode()->createChildSceneNode(actualSceneNodeName);

                meshReferenceDetails.sceneNode->attachObject(meshReferenceDetails.entity);
            }

            // load vertex groups
            string vertexGroupsFile = definition.getStringSetting("vertexGroupsFile", definitionGroupLabel);
            if (vertexGroupsFile.length()) {
                string resourceGroup = Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME;
                Ogre::ResourceGroupManager *resourceGroupManager = Ogre::ResourceGroupManager::getSingletonPtr();
                CLAssert(resourceGroupManager->resourceExists(resourceGroup, vertexGroupsFile), "Bad target vertex groups file in definition file");
                // read and parse
                Ogre::DataStreamPtr stream = resourceGroupManager->openResource(vertexGroupsFile, resourceGroup, true);
                Ogre::String blob = stream->getAsString();
                meshReferenceDetails.vertexGroups = CLGeometry::loadVertexGroups(blob);
            }

            string vertexGroupName = getVertexGroupName(definition, definitionGroupLabel);
            meshReferenceDetails.vertexGroupName = vertexGroupName;

            auto restBoneName = definition.getStringSetting("restBoneName", definitionGroupLabel);
            meshReferenceDetails.restBoneName = restBoneName;
        }

        return meshReferenceDetails;
    }

    string getVertexGroupName(const RTFConfigFile &definition, const string &sectionName)
    {
        auto vertexGroupName = definition.getStringSetting("vertexGroup", sectionName);
        // default to all vertices
        if (vertexGroupName.size() == 0) {
            vertexGroupName = CLDeduplicatedMesh::AllVerticesVertexGroupName;
        }
        return vertexGroupName;
    }


    static shared_ptr<CLDeduplicatedMesh>
    buildDeduplicatedMesh(Ogre::Entity *entity, bool useSkeletalData, string restBoneName, const map <string, set<size_t>> &vertexGroups, shared_ptr <CLDeduplicatedMeshManager> meshManager)
    {
        auto deduplicatedMesh = std::make_shared<CLDeduplicatedMesh>(entity, useSkeletalData, restBoneName, meshManager);
        CLAssert(deduplicatedMesh, "couldn't make dummy source");
        deduplicatedMesh->setVertexGroups(vertexGroups);
        return deduplicatedMesh;
    }



    static BuildResult
    buildDeduplicatedMesh(Ogre::SceneManager *sceneManager, shared_ptr <CLDeduplicatedMeshManager> meshManager, const RTFConfigFile& definition, const string &section, bool useSkeletalData)
    {
        auto meshParams = getMeshReferenceOrLoad(sceneManager, definition, section);
        auto mesh = buildDeduplicatedMesh(meshParams.entity, useSkeletalData, meshParams.restBoneName, meshParams.vertexGroups, meshManager);
        return {mesh, meshParams.sceneNode};
    }

    BuildResult
    buildDeduplicatedMesh(Ogre::SceneManager *sceneManager, shared_ptr <CLDeduplicatedMeshManager> meshManager, const RTFConfigFile& definition, const string &section)
    {
        auto useSkeletalData = definition.getBoolSetting("useSkeleton", section, false);
        return buildDeduplicatedMesh(sceneManager, meshManager, definition, section, useSkeletalData);
    }
}
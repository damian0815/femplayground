//
// Created by Damian Stewart on 03/05/2017.
//

#ifndef FEMPLAYGROUND_CLDEDUPLICATEDMESHFACTORY_H
#define FEMPLAYGROUND_CLDEDUPLICATEDMESHFACTORY_H

#include "RTFConfigFile.h"

class CLDeduplicatedMesh;
class CLDeduplicatedMeshManager;


namespace CLDeduplicatedMeshFactory
{
    struct BuildResult {
        shared_ptr< CLDeduplicatedMesh> mesh;
        Ogre::SceneNode* node;
    };

    string getVertexGroupName(const RTFConfigFile &definition, const string &sectionName);

    BuildResult buildDeduplicatedMesh(Ogre::SceneManager *sceneManager, shared_ptr <CLDeduplicatedMeshManager> meshManager, const RTFConfigFile& definition, const string &section);
};


#endif //FEMPLAYGROUND_CLDEDUPLICATEDMESHFACTORY_H

//
//  FPApp-ClothTestScene.cpp
//  
//
//  Created by Damian Stewart on 22/11/15.
//
//

#include "RTFConfigFile.h"
#include "FPApp.h"
#include "RTFNewFactory.h"
#include "RTFMultipleObjectUpdater.h"
#include "RTFClothObject.h"
#include "RTFTetraObject.h"
#include "RTFSpringConstraint.h"
#include "RTFWorldPositionConstraint.h"
#include "FPSliderHelpers.h"
#include "FPVertexPositionsSinkSourceHelpers.h"
#include "CLOgreUtil.h"


void FPApp::setupClothScene()
{
    static const string TAB_NAME = "Constraints";
    CLInterfaceWindowWrapper::addSlidersTab(TAB_NAME);

    auto updaterDefinition = "ClothUpdater.femUpdaterDefinition";
    
    auto cloth_4QuadsDefinition = "Cloth_4Quads.femClothDefinition";
    auto cloth2Definition = "ClothTest.femClothDefinition";
    //auto cloth3Definition = "Cloth_1Quad.femClothDefinition";
    auto cloth1Definition = "ClothTestB.femClothDefinition";
    auto cloth3Definition = "ClothTestC.femClothDefinition";
	
	auto tetra1Definition = "icoSphere1.femTetraDefinition.json";
    
	//addMesh("Cloth_4Quads", "Cloth_4Quads.mesh");
    
	//addMesh("Cloth_1Quad", "Cloth_1Quad.mesh");
    
    //addMesh("ClothTest", "/ClothTest.mesh");
    //addMesh("ClothTestB", "ClothTest.mesh");
    //addMesh("ClothTestC", "ClothTest.mesh");
    
    auto cloth1 = RTFNewFactory::buildClothObjectFromDefinition(cloth1Definition);
    auto cloth1Object = std::static_pointer_cast<RTFObject>(cloth1);
    mRTFSystem.addObject(cloth1Object);
    
    //auto cloth2 = RTFNewFactory::buildClothObjectFromDefinition(cloth2Definition);
    //auto cloth2Object = std::static_pointer_cast<RTFObject>(cloth2);
    //mRTFSystem.addObject(cloth2Object);
    
    auto cloth_4Quads = RTFNewFactory::buildClothObjectFromDefinition(cloth_4QuadsDefinition);
    auto cloth_4QuadsObject = std::static_pointer_cast<RTFObject>(cloth_4Quads);
    mRTFSystem.addObject(cloth_4QuadsObject);
    
    /*auto cloth3 = RTFNewFactory::buildClothObjectFromDefinition(cloth3Definition);
    auto cloth3Object = std::static_pointer_cast<RTFObject>(cloth3);
    mRTFSystem.addObject(cloth3Object);*/
	
	auto tetra1 = RTFNewFactory::buildTetgenTetraObjectFromDefinition(tetra1Definition);
	auto tetra1Object = std::static_pointer_cast<RTFObject>(tetra1);
	mRTFSystem.addObject(tetra1Object);
    
    auto updater = RTFNewFactory::buildMultipleObjectUpdaterFromDefinition({/*cloth2Object, *//*cloth3Object, cloth1Object, */tetra1Object, cloth_4QuadsObject}, updaterDefinition);
    
    //auto worldConstraint = updater->constrainVertexToWorld(cloth1Object, 0, 100, 0.1);
    //addWorldPositionConstraintSliders("c1 v0", worldConstraint);
    //auto worldConstraint = updater->constrainVertexToWorld(cloth1Object, 1, 100, 0.1);
    //addWorldPositionConstraintSliders("c1 v1", worldConstraint);
    updater->constrainVertexToWorldHard(tetra1Object.get(), 0);
    updater->constrainVertexToWorldHard(tetra1Object.get(), 1);
	
    //updater->constrainVertexToWorldHard(tetra1Object, 0);
    
    //updater->constrainVertexToWorldHard(cloth3Object, 1);
    
    //auto samePositionConstraint = RTFNewFactory::addSamePositionConstraint(updater, cloth1Object, 2, cloth2Object, 2, 100);
    //addSpringConstraintSlider("c1 to c2", samePositionConstraint, 1, 1000);
    //RTFNewFactory::addSamePositionConstraint(updater, cloth1Object, 3, cloth2Object, 3, 1000);
    //auto constraint2 = RTFNewFactory::addSamePositionConstraint(updater, cloth2Object, 1, cloth3Object, 1, 400);
    //RTFNewFactory::addSamePositionConstraint(updater, cloth2Object, 0, cloth3Object, 0, 1000);

	
    auto springConstraint = RTFNewFactory::addSpringConstraint(updater, tetra1Object.get(), 2, cloth_4QuadsObject.get(), 2, 100);
    addSpringConstraintSlider(TAB_NAME, "t1 to 4quads", springConstraint, 1, 1000);
    
    
    //springConstraint = RTFNewFactory::addSpringConstraint(updater, tetra1Object, 3, tetra1Object, 11, 1);
    
    //auto springConstraint = RTFNewFactory::addSpringConstraint(updater, cloth1Object, 20, tetra1Object, 1, 50);
    //addSpringConstraintSlider("c1 to t1", springConstraint, 1, 1000);
    //addSpringLengthSlider("c1 to t1 length", springConstraint, 0, 10);
	
    updater->buildIntegrator();
    
    // done
    mRTFSystem.addUpdater(updater);


    auto cloth1VertexPositionsSink = FPVertexPositionsSinkSourceHelpers::buildVertexPositionsSink(getSceneManager(), nullptr, RTFConfigFile(CLOgreUtil::findFilePath(cloth1Definition)), "Target");
    auto vertexMover1 = RTFNewFactory::buildOneToOneVertexMover(cloth1Object, cloth1VertexPositionsSink);
	mRTFSystem.addVertexMover(std::static_pointer_cast<RTFObjectVertexMover>(vertexMover1));
	
    //auto vertexMover2 = RTFNewFactory::buildVertexMover(OgreFramework::getSingletonPtr()->m_pSceneMgr, cloth2Object, cloth2Definition);
    //mRTFSystem.addVertexMover(vertexMover2);
    
    //auto vertexMover3 = RTFNewFactory::buildVertexMover(OgreFramework::getSingletonPtr()->m_pSceneMgr, cloth3Object, cloth3Definition);
    //mRTFSystem.addVertexMover(vertexMover3);

    auto cloth_4QuadsVertexPositionsSink = FPVertexPositionsSinkSourceHelpers::buildVertexPositionsSink(getSceneManager(), nullptr, RTFConfigFile(CLOgreUtil::findFilePath(cloth_4QuadsDefinition)), "Target");
    auto vertexMover_4Quads = RTFNewFactory::buildOneToOneVertexMover(cloth_4QuadsObject, cloth_4QuadsVertexPositionsSink);//, cloth_4QuadsObject, cloth_4QuadsDefinition);
	mRTFSystem.addVertexMover(std::static_pointer_cast<RTFObjectVertexMover>(vertexMover_4Quads));
}
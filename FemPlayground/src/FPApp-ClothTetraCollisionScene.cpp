//
//  FPApp-WallCollisionScene.cpp
//  
//
//  Created by Damian Stewart on 22/11/15.
//
//

#include "FPApp.h"
#include "RTFNewFactory.h"
#include "RTFMultipleObjectUpdater.h"
#include "RTFClothObject.h"
#include "RTFTetraObject.h"
#include "RTFWallSpatialLimitConstraint.h"
#include "RTFPlaneSpatialLimitConstraint.h"
#include "RTFCollisionConstraint.h"
#include "CLInterfaceWindowWrapper.h"
#include "FPSliderHelpers.h"
#include "RTFPenetrationDetector.h"
#include "TriangleTetraPenetrationDistanceEstimator.h"
#include "TetraSoup.h"
#include "FPClothTetraCollisionManager.h"
#include "RTFConfigFile.h"
#include "CLOgreUtil.h"
#include "FPVertexPositionsSinkSourceHelpers.h"

#include <glm/gtx/rotate_vector.hpp>

using glm::dvec3;

static shared_ptr<FPClothTetraCollisionManager> clothTetraCollisionManager = nullptr;

void FPApp::setupClothTetraCollisionScene()
{
    const string TAB = CLInterfaceWindowWrapper::DEFAULT_TAB;

    auto sphere1Definition = "icoSphere1.femTetraDefinition.json";
    auto clothDefinition = "ClothTest.femClothDefinition";

    auto sphere1 = RTFNewFactory::buildTetgenTetraObjectFromDefinition(sphere1Definition);
    mRTFSystem.addObject(sphere1);
    auto cloth = RTFNewFactory::buildClothObjectFromDefinition(clothDefinition);
    mRTFSystem.addObject(cloth);
   
    addTetraObjectMaterialSliders(TAB, "sphere1", sphere1);

    auto updaterDefinition = "WallCollisionTestUpdater.femUpdaterDefinition";
    auto updater = RTFNewFactory::buildMultipleObjectUpdaterFromDefinition({sphere1, /*sphere2,*/ cloth}, updaterDefinition);

    auto worldConstrainedVertices = RTFNewFactory::loadWorldConstrainedVerticesFromDefinition(clothDefinition, "Constrain");
    for (auto index: worldConstrainedVertices) {
        updater->removeVertexFromSimulation(updater->getGlobalVertexIndex(cloth.get(), index));

        //updater->constrainVertexToWorldPD(cloth.get(), (int)index, 100, 0.1);
    }

    // add constraints
    
    // spatial constraints
    auto allRestPositions = sphere1->getVertexPositions()->getRestPositions();
    allRestPositions.insert(allRestPositions.end(), cloth->getVertexPositions()->getRestPositions().begin(), cloth->getVertexPositions()->getRestPositions().end());

    vec3 planePoint(0,0.8,0);
    vec3 planeNormal(0,1,0);
    planeNormal = glm::rotateX(planeNormal, 0.5f);
    const float GROUND_PLANE_FORCE_FACTOR = 1e-3;
    const float GROUND_PLANE_DAMPING_FACTOR = 1e-3;
    auto groundPlaneConstraint1 = std::make_shared<RTFPlaneSpatialLimitConstraint>(allRestPositions, glm::rotateY(planeNormal, 90.0f), planePoint, GROUND_PLANE_FORCE_FACTOR, GROUND_PLANE_DAMPING_FACTOR);
    auto groundPlaneConstraint2 = std::make_shared<RTFPlaneSpatialLimitConstraint>(allRestPositions, glm::rotateY(planeNormal, 180.0f), planePoint, GROUND_PLANE_FORCE_FACTOR, GROUND_PLANE_DAMPING_FACTOR);
    auto groundPlaneConstraint3 = std::make_shared<RTFPlaneSpatialLimitConstraint>(allRestPositions, glm::rotateY(planeNormal, 270.0f), planePoint, GROUND_PLANE_FORCE_FACTOR, GROUND_PLANE_DAMPING_FACTOR);
    auto groundPlaneConstraint4 = std::make_shared<RTFPlaneSpatialLimitConstraint>(allRestPositions, planeNormal, planePoint, GROUND_PLANE_FORCE_FACTOR, GROUND_PLANE_DAMPING_FACTOR);
    
    updater->addConstraint(groundPlaneConstraint1);
    updater->addConstraint(groundPlaneConstraint2);
    updater->addConstraint(groundPlaneConstraint3);
    updater->addConstraint(groundPlaneConstraint4);

    clothTetraCollisionManager = std::make_shared<FPClothTetraCollisionManager>(updater.get(), cloth.get(), sphere1.get());

    updater->addExternalForceFunction([&] (const double* q, const double* qVel, double* f) {
        clothTetraCollisionManager->addExternalForces(q, qVel, f);
    });

    CLInterfaceWindowWrapper::addLogarithmicSlider(CLInterfaceWindowWrapper::DEFAULT_TAB, "Penetration F", clothTetraCollisionManager->getPenetrationRecoveryForceFactor(), 1e-1, 1e6, [&] (float value) {
        clothTetraCollisionManager->setPenetrationRecoveryForceFactor(value);
    });
    CLInterfaceWindowWrapper::addLogarithmicSlider(CLInterfaceWindowWrapper::DEFAULT_TAB, "Friction", clothTetraCollisionManager->getFrictionForceFactor(), 1e-2, 1e4, [&] (float value) {
        clothTetraCollisionManager->setFrictionForceFactor(value);
    });

    updater->buildIntegrator();

    auto sphere1VertexSink = FPVertexPositionsSinkSourceHelpers::buildVertexPositionsSink(getSceneManager(), nullptr, RTFConfigFile(CLOgreUtil::findFilePath(sphere1Definition)), "Target");
    auto sphere1VertexMover = RTFNewFactory::buildOneToOneVertexMover(sphere1, sphere1VertexSink);
    mRTFSystem.addVertexMover(sphere1VertexMover);

    auto clothVertexSink = FPVertexPositionsSinkSourceHelpers::buildVertexPositionsSink(getSceneManager(), nullptr, RTFConfigFile(CLOgreUtil::findFilePath(clothDefinition)), "Target");
    auto clothVertexMover = RTFNewFactory::buildOneToOneVertexMover(cloth, clothVertexSink);
    mRTFSystem.addVertexMover(clothVertexMover);

    // done
    mRTFSystem.addUpdater(updater);
    
    addPlaneSpatialLimitSliders(TAB, "ground", {groundPlaneConstraint1, groundPlaneConstraint2, groundPlaneConstraint3, groundPlaneConstraint4}, 1e3);


    setUpdateTimeFactor(0.0);

}



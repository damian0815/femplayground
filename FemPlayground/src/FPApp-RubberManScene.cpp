//
//  FPApp-RubberManScene.cpp
//  
//
//  Created by Damian Stewart on 22/11/15.
//
//

#include <src/CLInterfaceWindowWrapper.h>
#include "FPApp.h"
#include "RTFNewFactory.h"
#include "RTFMultipleObjectUpdater.h"
#include "RTFClothObject.h"
#include "RTFTetraObject.h"
#include "CLAnimatedOgreEntity.h"
#include "FPOgreAnimatedMeshVertexPositionsSource.h"
#include "RTFConfigFile.h"
#include "FPVertexPositionsSinkSourceHelpers.h"
#include "CLOgreUtil.h"

static void addRubberManSliders(RTFTetraObject* armObject)
{
    CLInterfaceWindowWrapper::addLogarithmicSlider(CLInterfaceWindowWrapper::DEFAULT_TAB, "fem E", armObject->getMaterialYoungsModulus(), 0.1f, 1e5,
                                                   [armObject] (float value) {
                                                      armObject->setMaterialYoungsModulus(value);
                                                   });
}

void FPApp::setupRubberManScene()
{
    auto updaterDefinition = "RubberManUpdater.femUpdaterDefinition.json";
	
	auto armyMan = addAnimatedEntity("ArmyMan", "ArmyMan.mesh");
	armyMan->setAnimation("WaveArms", true);
	
    auto rubberManArmDefinition = "RubberManArm.femTetraDefinition.json";
    auto rubberManArmObject = RTFNewFactory::buildTetgenTetraObjectFromDefinition(CLOgreUtil::findFilePath(rubberManArmDefinition));
    mRTFSystem.addObject(std::static_pointer_cast<RTFObject>(rubberManArmObject));
    addRubberManSliders(rubberManArmObject.get());
    
    auto icoSphereDefinition = "icoSphere1.femTetraDefinition.json";
   	auto icoSphere = RTFNewFactory::buildTetgenTetraObjectFromDefinition(icoSphereDefinition);
	auto icoSphereObject = std::static_pointer_cast<RTFObject>(icoSphere);
	mRTFSystem.addObject(icoSphereObject);
    
    auto updater = RTFNewFactory::buildMultipleObjectUpdaterFromDefinition({rubberManArmObject, icoSphereObject}, updaterDefinition);

    auto rubberManArmVertexPositionsSource = FPVertexPositionsSinkSourceHelpers::buildVertexPositionsSource(getSceneManager(), mDeduplicatedMeshManager, RTFConfigFile(CLOgreUtil::findFilePath(rubberManArmDefinition)), "MeshConstraint");
    auto meshConstraint = RTFNewFactory::buildMeshConstraint(rubberManArmObject, updater->getGlobalStartVertexIndex(rubberManArmObject.get()), rubberManArmVertexPositionsSource, rubberManArmDefinition, "MeshConstraint");

    updater->addConstraint(std::static_pointer_cast<RTFConstraint>(meshConstraint));
    
    updater->buildIntegrator();
    mRTFSystem.addUpdater(updater);

    auto rubberManArmVertexPositionsSink = FPVertexPositionsSinkSourceHelpers::buildVertexPositionsSink(getSceneManager(), mDeduplicatedMeshManager, RTFConfigFile(CLOgreUtil::findFilePath(rubberManArmDefinition)), "Target");
    auto vertexMover = RTFNewFactory::buildDeformCageVertexMover(rubberManArmObject, rubberManArmVertexPositionsSink);
	mRTFSystem.addVertexMover(std::static_pointer_cast<RTFObjectVertexMover>(vertexMover));
}


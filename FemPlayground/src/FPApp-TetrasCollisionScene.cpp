//
//  FPApp-TetrasCollisionScene.cpp
//  
//
//  Created by Damian Stewart on 26/11/15.
//
//

#include "FPApp.h"
#include "RTFNewFactory.h"
#include "RTFMultipleObjectUpdater.h"
#include "RTFClothObject.h"
#include "RTFTetraObject.h"
#include "RTFWallSpatialLimitConstraint.h"
#include "RTFCollisionConstraint.h"
#include "CLInterfaceWindowWrapper.h"
#include "FPSliderHelpers.h"
#include "TetMesh.h"
#include "RTFPenetrationDetector.h"

using glm::dvec3;


void FPApp::setupTetrasCollisionScene()
{
    static const string TAB_NAME = "Constraints";
    CLInterfaceWindowWrapper::addSlidersTab(TAB_NAME);

    auto tetrasDefinition = "tetras.femTetraDefinition";
    
    auto tetras = RTFNewFactory::buildTetgenTetraObjectFromDefinition(tetrasDefinition);
    mRTFSystem.addObject(tetras);

    addTetraObjectMaterialSliders(TAB_NAME, "tetras", tetras);
    
    auto updaterDefinition = "tetraCollisionTestUpdater.femUpdaterDefinition";
    auto updater = RTFNewFactory::buildMultipleObjectUpdaterFromDefinition({tetras}, updaterDefinition);
    
    // add constraints
    
    // spatial constraints
    auto allRestPositions = tetras->getVertexPositions()->getRestPositions();
    auto groundPlaneConstraint = std::make_shared<RTFWallSpatialLimitConstraint>(allRestPositions, RTFWallSpatialLimitConstraint::AXIS_Y_NEGATIVE, 0.0, 1e5, 1e0);
    updater->addConstraint(groundPlaneConstraint);
    
    
    // collision detector
    unsigned int totalTetraCount = tetras->getTetMesh()->getNumElements();
    RTFPenetrationDetector::Params penetrationDetectorParams({
            .spatialHashParams = SpatialHash::Params {
                    .cellSize = 0.5, // todo: average length of tetra edge
                    .size = totalTetraCount * 8
            },
            .ignoreSelfPenetrations = false
    });
    DistanceField::Params distanceFieldParams {
        .gridSize = 0.1,
        .maxOuterDistance = 0.02
    };
    auto collisionConstraint = std::make_shared<RTFCollisionConstraint>(penetrationDetectorParams);
    collisionConstraint->registerFEMTetraObject(tetras, distanceFieldParams, updater->getGlobalStartVertexIndex(tetras.get()));
    updater->addConstraint(collisionConstraint);
    
    
    
    updater->buildIntegrator();
    
    //auto sphere1VertexMover = RTFNewFactory::buildOneToOneVertexMover(getSceneManager(), sphere1, sphere1Definition);
    //mRTFSystem.addVertexMover(sphere1VertexMover);
    
    // done
    mRTFSystem.addUpdater(updater);

    addWallSpatialLimitSliders(TAB_NAME, "ground", groundPlaneConstraint, 1e5);
    addCollisionSliders(TAB_NAME, "collisions", collisionConstraint, 1e2);
    
}



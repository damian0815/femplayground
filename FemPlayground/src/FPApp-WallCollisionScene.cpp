//
//  FPApp-WallCollisionScene.cpp
//  
//
//  Created by Damian Stewart on 22/11/15.
//
//

#include "FPApp.h"
#include "RTFNewFactory.h"
#include "RTFMultipleObjectUpdater.h"
#include "RTFClothObject.h"
#include "RTFTetraObject.h"
#include "RTFWallSpatialLimitConstraint.h"
#include "RTFPlaneSpatialLimitConstraint.h"
#include "RTFCollisionConstraint.h"
#include "CLInterfaceWindowWrapper.h"
#include "FPSliderHelpers.h"
#include "TetMesh.h"
#include "RTFPenetrationDetector.h"
#include "RTFConfigFile.h"
#include "CLOgreUtil.h"
#include "FPVertexPositionsSinkSourceHelpers.h"

#include <glm/gtx/rotate_vector.hpp>

using glm::dvec3;


void FPApp::setupWallCollisionScene()
{
    static const string TAB_NAME = "Constraints";
    CLInterfaceWindowWrapper::addSlidersTab(TAB_NAME);

    auto sphere1Definition = "icoSphere1.femTetraDefinition.json";
    //auto sphere2Definition = "icoSphere2.femTetraDefinition";
    auto planeDefinition = "plane-solid.femTetraDefinition";
    
    auto sphere1 = RTFNewFactory::buildTetgenTetraObjectFromDefinition(sphere1Definition);
    mRTFSystem.addObject(sphere1);
    //auto sphere2 = RTFNewFactory::buildTetgenTetraObjectFromDefinition(sphere2Definition);
    //mRTFSystem.addObject(sphere2);
    auto plane = RTFNewFactory::buildTetgenTetraObjectFromDefinition(planeDefinition);
    mRTFSystem.addObject(plane);


    addTetraObjectMaterialSliders(TAB_NAME, "sphere1", sphere1);
    addTetraObjectMaterialSliders(TAB_NAME, "plane", plane, 1e10);

    //sphere2->moveVertexRestPositions(dvec3(1,0,0));
    
    auto updaterDefinition = "WallCollisionTestUpdater.femUpdaterDefinition";
    auto updater = RTFNewFactory::buildMultipleObjectUpdaterFromDefinition({sphere1, /*sphere2,*/ plane}, updaterDefinition);

    auto worldConstrainedVertices = RTFNewFactory::loadWorldConstrainedVerticesFromDefinition(planeDefinition, "Constrain");
    for (auto index: worldConstrainedVertices) {
        updater->constrainVertexToWorldPD(plane.get(), (int)index, 1000, 1);
    }

    // add constraints
    
    // spatial constraints
    auto allRestPositions = sphere1->getVertexPositions()->getRestPositions();
    //allRestPositions.insert(allRestPositions.end(), sphere2->getVertexPositions()->getRestPositions().begin(), sphere2->getVertexPositions()->getRestPositions().end());
    allRestPositions.insert(allRestPositions.end(), plane->getVertexPositions()->getRestPositions().begin(), plane->getVertexPositions()->getRestPositions().end());
    //auto groundPlaneConstraint = std::make_shared<RTFWallSpatialLimitConstraint>(allRestPositions, RTFWallSpatialLimitConstraint::AXIS_Y_NEGATIVE, 0.95, 1e3, 1e0);
    
    dvec3 planePoint(0,0.8,0);
    dvec3 planeNormal(0,1,0);
    planeNormal = glm::rotateX(planeNormal, 0.5);
    auto groundPlaneConstraint1 = std::make_shared<RTFPlaneSpatialLimitConstraint>(allRestPositions, glm::rotateY(planeNormal, 90.0), planePoint, 1e3, 1e0);
    auto groundPlaneConstraint2 = std::make_shared<RTFPlaneSpatialLimitConstraint>(allRestPositions, glm::rotateY(planeNormal, 180.0), planePoint, 1e3, 1e0);
    auto groundPlaneConstraint3 = std::make_shared<RTFPlaneSpatialLimitConstraint>(allRestPositions, glm::rotateY(planeNormal, 270.0), planePoint, 1e3, 1e0);
    auto groundPlaneConstraint4 = std::make_shared<RTFPlaneSpatialLimitConstraint>(allRestPositions, planeNormal, planePoint, 1e3, 1e0);
    
    updater->addConstraint(groundPlaneConstraint1); 
    updater->addConstraint(groundPlaneConstraint2);
    updater->addConstraint(groundPlaneConstraint3);
    updater->addConstraint(groundPlaneConstraint4);
    
    // collision constraint
    RTFPenetrationDetector::Params penetrationDetectorParams {
            .spatialHashParams = {
                    .cellSize = 0.025f,
                    .size = 10000
            },
            .ignoreSelfPenetrations = true
    };
    auto collisionConstraint = std::make_shared<RTFCollisionConstraint>(penetrationDetectorParams);

    DistanceField::Params distanceFieldParams {
        .gridSize = 0.05f,
        .maxOuterDistance = 0.02f
    };
    collisionConstraint->registerFEMTetraObject(sphere1, distanceFieldParams, updater->getGlobalStartVertexIndex(sphere1.get()));
    //collisionConstraint->registerFEMTetraObject(sphere2, distanceFieldParams, updater->getGlobalStartVertexIndex(sphere2));
    collisionConstraint->registerFEMTetraObject(plane, distanceFieldParams, updater->getGlobalStartVertexIndex(plane.get()));
    updater->addConstraint(collisionConstraint);
    
    updater->buildIntegrator();

    auto sphere1VertexPositionsSink = FPVertexPositionsSinkSourceHelpers::buildVertexPositionsSink(getSceneManager(), nullptr, RTFConfigFile(CLOgreUtil::findFilePath(sphere1Definition)), "Target");
    auto sphere1VertexMover = RTFNewFactory::buildOneToOneVertexMover(sphere1, sphere1VertexPositionsSink);

    //auto sphere2VertexMover = RTFNewFactory::buildOneToOneVertexMover(getSceneManager(), sphere2, sphere2Definition);
    auto planeVertexPositionsSink = FPVertexPositionsSinkSourceHelpers::buildVertexPositionsSink(getSceneManager(), nullptr, RTFConfigFile(CLOgreUtil::findFilePath(planeDefinition)), "Target");
    auto planeVertexMover = RTFNewFactory::buildOneToOneVertexMover(plane, planeVertexPositionsSink);
    mRTFSystem.addVertexMover(sphere1VertexMover);
    //mRTFSystem.addVertexMover(sphere2VertexMover);
    mRTFSystem.addVertexMover(planeVertexMover);
    
    // done
    mRTFSystem.addUpdater(updater);

    addPlaneSpatialLimitSliders(TAB_NAME, "ground", {groundPlaneConstraint1, groundPlaneConstraint2,
                                                          groundPlaneConstraint3, groundPlaneConstraint4}, 1e3);

    addCollisionSliders(TAB_NAME, "collision", collisionConstraint, 1e8);
    
    
    
}



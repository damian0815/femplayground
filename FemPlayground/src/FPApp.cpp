//
//  FPApp.cpp
//  RubberMan
//
//  Created on 25/02/15.
//
//

#include "FPApp.h"

#include "RTFNewFactory.h"
#include "RTFMultipleObjectUpdater.h"
#include "RTFZeroLengthSpringConstraint.h"
#include "CLInterfaceWindowWrapper.h"
#include "RTFSpringConstraint.h"
#include "RTFWorldPositionConstraint.h"
#include "CLAppNotifications.h"
#include "CLNotificationCenter.h"

#include "RTFClothObject.h"
#include "RTFTetraObject.h"
#include "CLAnimatedOgreEntity.h"

#include <Ogre/OgreAnimationState.h>
#include "CLOgreUtil.h"
#include "FPOgreRTFDebugDrawerAdapter.h"

FPApp::FPApp()
{
	RTFNewFactory::findFilePathHandler = [] (string filename) {
		return CLOgreUtil::findFilePath(filename);
	};

	mDeduplicatedMeshManager = std::make_shared<CLDeduplicatedMeshManager>();
}

void FPApp::setupFEMSystem()
{
    setupRubberManScene();
    //setupClothScene();
    //setupWallCollisionScene();
    //setupTetrasCollisionScene();
	//setupClothTetraCollisionScene();
    
    CLNotificationCenter::get()->addObserver(CLPhysicsNotificationGravityChanged, [this](const CLGenericDictionary& info) {
        for (int i=0; i<mRTFSystem.getNumUpdaters(); i++) {
            float gY = boost::get<float>(info.at(CLGravityKey));
            mRTFSystem.getUpdater(i)->setGravity(dvec3(0, -gY, 0));
        }
    });
}

shared_ptr<CLAnimatedOgreEntity> FPApp::addAnimatedEntity(string name, string meshName)
{
	auto node = addMesh(name, meshName);
	node->setInitialState();
	auto entity = (Ogre::Entity*)node->getAttachedObject(0);
	auto wrapper = std::make_shared<CLAnimatedOgreEntity>(entity, node);
	mAnimatedEntities.push_back(wrapper);
	return wrapper;
}


void FPApp::reset()
{
    CLApp::reset();
	
	for (auto entity: mAnimatedEntities) {
		entity->getNode()->resetToInitialState();
	}


    mRTFSystem.reset();
}


void FPApp::update(float dtWall, float dtWorld)
{
    CLApp::update(dtWall, dtWorld);
	
	
	for (auto entity: mAnimatedEntities) {
		entity->getNode()->_setDerivedPosition(Ogre::Vector3(sinf(mTotalElapsedWorldTime), 0, 0));
	}
	
	updateAnimatedEntities(dtWorld);


    mRTFSystem.update(dtWorld);
}

void FPApp::updateAnimatedEntities(float dt)
{
	mDeduplicatedMeshManager->clFrameTick(dt);
}

void FPApp::debugDraw(OgreDebugDrawer* drawer)
{
    CLApp::debugDraw(drawer);
    if (mDoDebugDraw) {
        FPOgreRTFDebugDrawerAdapter debugDrawerAdapter(drawer);
        mRTFSystem.debugDraw(&debugDrawerAdapter);
    }
}

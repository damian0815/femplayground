//
//  FPApp.h
//  RubberMan
//
//  Created on 25/02/15.
//
//

#ifndef __RubberMan__FPApp__
#define __RubberMan__FPApp__

#include <iostream>
#include <src/CLDeduplicatedMeshManager.h>
#include "CLApp.h"
#include "RTFSystem.h"

class CLAnimatedOgreEntity;

class FPApp: public CLApp
{
public:
	FPApp();
    
	
protected:
	// override point for frame updates
	virtual void update(float dtWall, float dtWorld) override;
    
    virtual void reset() override;
    
    virtual void debugDraw(OgreDebugDrawer* drawer) override;
    
    virtual bool shouldBuildCartwheel() override { return false; }
	
	virtual void setupFEMSystem() override;
    
    
    
private:
	
	shared_ptr<CLAnimatedOgreEntity> addAnimatedEntity(string name, string meshFileName);
	void updateAnimatedEntities(float dt);
    
    void setupClothScene();
    void setupRubberManScene();
    void setupWallCollisionScene();
    void setupTetrasCollisionScene();
    void setupClothTetraCollisionScene();

	
    RTFSystem mRTFSystem;
	shared_ptr<CLDeduplicatedMeshManager> mDeduplicatedMeshManager;

	vector<shared_ptr<CLAnimatedOgreEntity>> mAnimatedEntities;


};



#endif /* defined(__RubberMan__FPApp__) */

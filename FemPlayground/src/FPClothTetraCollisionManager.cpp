//
// Created by Damian Stewart on 03/10/16.
//

#include "TriangleTetraPenetrationDistanceEstimator.h"
#include "TetMesh.h"
#include "FPSliderHelpers.h"
#include "RTFCollisionConstraint.h"
#include "RTFWallSpatialLimitConstraint.h"
#include "RTFClothObject.h"
#include "RTFMultipleObjectUpdater.h"
#include "RTFNewFactory.h"
#include "FPApp.h"
#include "FPClothTetraCollisionManager.h"
#include "TetraTriangleIntersect.h"
#include "RTFUtil.h"

static std::vector<int> getTriangles(RTFClothObject* cloth)
{
    auto surfaceTriangles = cloth->getSurfaceTriangles();
    return std::vector<int>(surfaceTriangles.begin(), surfaceTriangles.end());
}

static std::vector<int> getTetras(RTFTetraObject* tetraObject)
{
    int numTetras = tetraObject->getTetMesh()->getNumElements();
    std::vector<int> tetraTetras;
    tetraTetras.reserve((size_t)numTetras);
    for (int i=0; i<numTetras; i++) {
        std::vector<vec3> thisTetraNodes;
        std::vector<int> thisTetraIndices;
        for (int j = 0; j < 4; j++) {
            auto t = tetraObject->getTetMesh()->getVertexIndex(i, j);
            thisTetraIndices.push_back(t);
            auto v = *tetraObject->getTetMesh()->getVertex(i, j);
            thisTetraNodes.push_back(vec3(v[0], v[1], v[2]));
        }

        if (!TetraOperations::AreTetraVerticesCorrectlyOrdered(&thisTetraNodes[0])) {
            TetraOperations::SwapVertexOrder(&thisTetraIndices[0]);
        }
        tetraTetras.insert(tetraTetras.end(), thisTetraIndices.begin(), thisTetraIndices.end());
    }
    return tetraTetras;
}

static vector<size_t> GetSurfaceFaceNodeIndices(const TetraSoup& tetraSoup, const TetraConnectivityInfo& tetraConnectivityInfo)
{
    auto surfaceFaceIndices = tetraConnectivityInfo.GetSurfaceFaces();
    vector<size_t> nodeIndices;
    nodeIndices.resize(surfaceFaceIndices.size() * 3);

    for (const auto& faceIndex: surfaceFaceIndices) {
        auto face = tetraSoup.GetIndexFace(faceIndex);
        nodeIndices.push_back(face[0]);
        nodeIndices.push_back(face[1]);
        nodeIndices.push_back(face[2]);
    }

    return nodeIndices;
}

InterpolatingDistanceField::DistanceField::Params GetDistanceFieldParams(const TetraSoup& tetraSoup)
{
    auto tetraAABB = InterpolatingDistanceField::CalculateAABB(tetraSoup.GetNodes());

    InterpolatingDistanceField::DistanceField::Params distanceFieldParams;
    distanceFieldParams.gridSize = std::max(tetraAABB.max.x - tetraAABB.min.x,
                                            std::max(tetraAABB.max.y - tetraAABB.min.y,
                                                     tetraAABB.max.z - tetraAABB.min.z)) / 20.0f;
    distanceFieldParams.maxOuterDistance = distanceFieldParams.gridSize * 5;
    return distanceFieldParams;

}


FPClothTetraCollisionManager::FPClothTetraCollisionManager(RTFMultipleObjectUpdater *updater, RTFClothObject *clothObject, RTFTetraObject *tetraObject)
        : mTriangleSoup(clothObject->getVertexPositions()->getRestPositions(), getTriangles(clothObject)),
        mTriangleConnectivityInfo(mTriangleSoup.GetTriangles()),
        mTetraSoup(tetraObject->getVertexPositions()->getRestPositions(), getTetras(tetraObject)),
        mTetraConnectivityInfo(mTetraSoup.GetTetras()),
          mDistanceField(mTetraSoup.GetNodes(), GetSurfaceFaceNodeIndices(mTetraSoup, mTetraConnectivityInfo), GetDistanceFieldParams(mTetraSoup))
{
    mClothVertexIndexOffset = updater->getGlobalStartVertexIndex(clothObject);
    mTetraVertexIndexOffset = updater->getGlobalStartVertexIndex(tetraObject);

    mClothRestPositions = clothObject->getVertexPositions()->getRestPositions();
    mTetraRestPositions = tetraObject->getVertexPositions()->getRestPositions();
}

void FPClothTetraCollisionManager::addExternalForces(const double* q, const double* qVel, double* f) __attribute__ ((optnone))
{
    vector<vec3> triangleNodes(mTriangleSoup.GetNumNodes());
    for (int i = 0; i < mTriangleSoup.GetNumNodes(); i++) {
        auto vGlobalIdx = i + mClothVertexIndexOffset;
        auto qIdx = vGlobalIdx * 3;
        auto v = vec3(mClothRestPositions.at(i)) + vec3(q[qIdx + 0], q[qIdx + 1], q[qIdx + 2]);
        triangleNodes[i] = v;
    }
    vector<vec3> tetraNodes(mTetraSoup.GetNumNodes());
    for (int i = 0; i < mTetraSoup.GetNumNodes(); i++) {
        auto vGlobalIdx = i + mTetraVertexIndexOffset;
        auto qIdx = vGlobalIdx * 3;
        auto v = vec3(mTetraRestPositions.at(i)) + vec3(q[qIdx + 0], q[qIdx + 1], q[qIdx + 2]);
        tetraNodes[i] = v;
    }

    mTetraSoup.UpdateNodePositions(tetraNodes);
    mTriangleSoup.UpdateNodePositions(triangleNodes);

    auto penetrationSurfaces = TetraTriangleIntersect::FindPenetrationSurfaces(mTriangleSoup, mTetraSoup);
    for (const auto& penetrationInfo: penetrationSurfaces) {

        auto nodeTriangleCurr = mTriangleSoup.GetNodeTriangle(penetrationInfo.triangleIndex);
        vec3 barycentricTri = TriangleOperations::ComputeBarycentric(nodeTriangleCurr, penetrationInfo.pos);

        auto nodeTetraCurr = mTetraSoup.GetNodeTetra(penetrationInfo.tetraIndex);
        vec4 barycentricTetra = TetraOperations::ComputeBarycentric(nodeTetraCurr, penetrationInfo.pos);

        auto indexTetra = mTetraSoup.GetIndexTetra(penetrationInfo.tetraIndex);
        auto indexTriangle = mTriangleSoup.GetIndexTriangle(penetrationInfo.triangleIndex);
        auto nodeTetraRest = TetraTempl<vec3>::BuildNodeTetra(indexTetra, mTetraRestPositions);
        auto penetrationPosRest = TetraOperations::ComputeCartesian(nodeTetraRest, barycentricTetra);

        auto distanceFieldData = mDistanceField.GetInterpolatedData(penetrationPosRest);
        auto surfacePosBarycentric = TetraOperations::ComputeBarycentric(nodeTetraRest, distanceFieldData.nearestSurfacePoint);
        auto surfacePosCurrent = TetraOperations::ComputeCartesian(nodeTetraCurr, surfacePosBarycentric);
        auto surfaceDirection = glm::normalize(surfacePosCurrent - penetrationInfo.pos);

        auto penetrationForce = mPenetrationRecoveryForceFactor * surfaceDirection * penetrationInfo.area * distanceFieldData.distance;


        vec3 tetraVelocity;
        for (int i=0; i<4; i++) {
            tetraVelocity += RTFUtil::array3DGet<double, vec3>(qVel, indexTetra[i]+mTetraVertexIndexOffset);
        }
        tetraVelocity /= 4;

        vec3 triangleVelocity;
        for (int i=0; i<3; i++) {
            triangleVelocity += RTFUtil::array3DGet<double, vec3>(qVel, indexTriangle[i]+mClothVertexIndexOffset);
        }
        triangleVelocity /= 3;

        auto triangleVelativeVelocityRelativeToTetra = triangleVelocity - tetraVelocity;
        auto perpendicularVelocity = glm::dot(surfaceDirection, triangleVelativeVelocityRelativeToTetra);
        auto parallelVelocity = triangleVelativeVelocityRelativeToTetra - perpendicularVelocity;

        auto frictionForce = -parallelVelocity * mFrictionForceFactor;

        for (int i=0; i<4; i++) {
            auto thisNodeForce = barycentricTetra[i]*(penetrationForce - frictionForce);
            RTFUtil::array3DAdd(f, indexTetra[i]+mTetraVertexIndexOffset, thisNodeForce);
        }

        for (int i=0; i<3; i++) {
            auto thisNodeForce = barycentricTri[i]*(-penetrationForce /*- frictionForce*/);
            RTFUtil::array3DAdd(f, indexTriangle[i]+mClothVertexIndexOffset, thisNodeForce);
        }
    }

    /*
    for (int i = 0; i < mTriangleNodes.size(); i++) {
        auto vIdx = i + mClothVertexIndexOffset;
        auto qIdx = vIdx * 3;

        auto depth = mPenetrationDepths[i];
        if (depth > 0) {
            CLUtil::array3DAdd(f, vIdx, depth * mPenetrationDirections[i] * mPenetrationRecoveryForceFactor);
        }
    }*/
}

vec3 FPClothTetraCollisionManager::CalculateFrictionForce(const double *qVel, int nodeQIdx, const vec3 &surfaceNormal) const
{
    auto thisNodeVelocity = RTFUtil::array3DGet<double, vec3>(qVel, nodeQIdx);
    auto velocityPerpendicularToSurface = dot(surfaceNormal, thisNodeVelocity);
    auto velocityParallelToSurface = thisNodeVelocity - velocityPerpendicularToSurface;
    auto thisNodeFriction = -velocityParallelToSurface * mFrictionForceFactor;
    return thisNodeFriction;
}


//
// Created by Damian Stewart on 03/10/16.
//

#ifndef FEMPLAYGROUND_CLOTHTETRACOLLISIONMANAGER_H
#define FEMPLAYGROUND_CLOTHTETRACOLLISIONMANAGER_H

#include <vector>
#include <glm/glm.hpp>
#include "TetraSoup.h"
#include "TriangleSoup.h"

using std::vector;

class FPClothTetraCollisionManager
{
public:
    FPClothTetraCollisionManager(RTFMultipleObjectUpdater *updater, RTFClothObject *clothObject, RTFTetraObject *tetraObject);
    void addExternalForces(const double* q, const double* qVel, double* f);

    float getPenetrationRecoveryForceFactor() { return mPenetrationRecoveryForceFactor; }
    void setPenetrationRecoveryForceFactor(float factor) { mPenetrationRecoveryForceFactor = factor; }

    float getFrictionForceFactor() { return mFrictionForceFactor; }
    void setFrictionForceFactor(float factor) { mFrictionForceFactor = factor; }
private:
    TriangleSoup mTriangleSoup;
    TriangleConnectivityInfo mTriangleConnectivityInfo;
    vector<vec3> mClothRestPositions;

    TetraSoup mTetraSoup;
    TetraConnectivityInfo mTetraConnectivityInfo;
    vector<vec3> mTetraRestPositions;

    InterpolatingDistanceField::DistanceField mDistanceField;

    float mPenetrationRecoveryForceFactor = 10000.0f;
    float mFrictionForceFactor = 1.0f;
    int mClothVertexIndexOffset, mTetraVertexIndexOffset;

    vec3 CalculateFrictionForce(const double *qVel, int nodeQIdx, const vec3 &surfaceNormal) const;

};


#endif //FEMPLAYGROUND_CLOTHTETRACOLLISIONMANAGER_H

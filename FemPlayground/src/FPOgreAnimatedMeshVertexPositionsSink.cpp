//
// Created by Damian Stewart on 03/05/2017.
//

#include "FPOgreAnimatedMeshVertexPositionsSink.h"
#include "CLUtil.h"
#include "xassert.h"

FPOgreAnimatedMeshVertexPositionsSink::FPOgreAnimatedMeshVertexPositionsSink(shared_ptr<CLDeduplicatedMesh> mesh, Ogre::SceneNode *node, const string vertexGroup)
: mMesh(mesh), mNode(node), mVertexGroup(vertexGroup), mTetMeshSourceOffset(0,0,0)
{
    CLAssert(mMesh->hasVertexGroup(vertexGroup), "Unrecognized vertex group");
	mMesh->clearBoneAssignmentsIfNecessary(vertexGroup);
}

Ogre::Matrix4 FPOgreAnimatedMeshVertexPositionsSink::getWorldToSceneNodeTransform()
{
    Ogre::Matrix4 transformMatrix = mNode->_getFullTransform().inverse();
    // include mTetMeshSourceOffset
    Ogre::Matrix4 tetMeshSourceOffsetTranslate;
    tetMeshSourceOffsetTranslate.makeTrans(-mTetMeshSourceOffset);
    transformMatrix = transformMatrix * tetMeshSourceOffsetTranslate;
    return transformMatrix;
}

vector<vec3> FPOgreAnimatedMeshVertexPositionsSink::getRestWorldPositions()
{
    auto sourceVertexPositionsRawPair = mMesh->getDeduplicatedVerticesInRestState(mVertexGroup);
    const auto& sourceVertexPositionsRaw = sourceVertexPositionsRawPair.positions;
    // convert to vec3
    vector<vec3> positions(sourceVertexPositionsRawPair.deduplicatedIndices.size());
    for ( size_t i=0; i<positions.size(); i++ ) {
        positions[i] = CLUtil::vector3DGet<double, vec3>(sourceVertexPositionsRaw, (int)i);
    }

    return positions;
}

void FPOgreAnimatedMeshVertexPositionsSink::setWorldPositions(const vector<vec3> &positions)
{
    DASSERT(positions.size() == getNumVertices());

    auto worldToSceneNodeTransform = getWorldToSceneNodeTransform();

    auto numVertices = getNumVertices();
    // transform positions into target scene node's space
    vector<Ogre::Vector3> localPositions(numVertices);
    for ( size_t i=0; i<numVertices; i++ ) {
        const auto& v = positions[i];
        auto localV = worldToSceneNodeTransform * Ogre::Vector3(v.x, v.y, v.z);
        localPositions[i] = localV;
    }

    // pass on to ogre
    mMesh->setVertexPositions(localPositions, mVertexGroup);
}

size_t FPOgreAnimatedMeshVertexPositionsSink::getNumVertices()
{
    return mMesh->getDeduplicatedVerticesCount(mVertexGroup);
}

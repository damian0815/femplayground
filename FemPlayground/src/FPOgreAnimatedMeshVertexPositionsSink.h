//
// Created by Damian Stewart on 03/05/2017.
//

#ifndef FEMPLAYGROUND_FPOGREANIMATEDMESHVERTEXPOSITIONSSINK_H
#define FEMPLAYGROUND_FPOGREANIMATEDMESHVERTEXPOSITIONSSINK_H

#include "IRTFVertexPositionsSink.h"
#include "CLDeduplicatedMesh.h"

class FPOgreAnimatedMeshVertexPositionsSink: public IRTFVertexPositionsSink
{
public:
    FPOgreAnimatedMeshVertexPositionsSink(shared_ptr<CLDeduplicatedMesh> mesh, Ogre::SceneNode* node, const string vertexGroup);

    vector<vec3> getRestWorldPositions() override;
    void setWorldPositions(const vector<vec3> &positions) override;
    size_t getNumVertices() override;

private:



    shared_ptr<CLDeduplicatedMesh> mMesh;
    Ogre::SceneNode* mNode;
    const string mVertexGroup;

    // TODO remove this
    Ogre::Vector3 mTetMeshSourceOffset;

    Ogre::Matrix4 getWorldToSceneNodeTransform();
};


#endif //FEMPLAYGROUND_FPOGREANIMATEDMESHVERTEXPOSITIONSSINK_H

//
// Created by Damian Stewart on 03/05/2017.
//

#include "CLUtil.h"
#include "FPOgreAnimatedMeshVertexPositionsSource.h"

FPOgreAnimatedMeshVertexPositionsSource::FPOgreAnimatedMeshVertexPositionsSource(shared_ptr<CLDeduplicatedMesh> mesh, Ogre::SceneNode *node, const string vertexGroup)
: mMesh(mesh), mNode(node), mVertexGroup(vertexGroup)
{

}

vector<vec3> FPOgreAnimatedMeshVertexPositionsSource::getRestWorldPositions()
{
    auto sourceVertexPositionsRawPair = mMesh->getDeduplicatedVerticesInRestState(mVertexGroup);
    const auto& sourceVertexPositionsRaw = sourceVertexPositionsRawPair.positions;
    // convert to vec3
    vector<vec3> positions(sourceVertexPositionsRawPair.deduplicatedIndices.size());
    for ( size_t i=0; i<positions.size(); i++ ) {
        positions[i] = CLUtil::vector3DGet<double, vec3>(sourceVertexPositionsRaw, (int)i);
    }

    return positions;
}

vector<vec3> FPOgreAnimatedMeshVertexPositionsSource::getCurrentWorldPositions()
{
    // calculate source vertex positions from Ogre mesh to pass to FEM
    auto positionsOgre = *(mMesh->getVertexPositions(mVertexGroup));

    // positionsOgre are in mMesh's space, so convert to world space
    vector<vec3> positions(positionsOgre.size());
    Ogre::Matrix4 transformMatrix = mNode->_getFullTransform();
    int counter = 0;
    for ( auto& it: positionsOgre ) {
        Ogre::Vector3& v = it.second;
        v = transformMatrix*v;
        positions[counter] = vec3(v.x, v.y, v.z);
        ++counter;
    }

    return positions;
}



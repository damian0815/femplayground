//
// Created by Damian Stewart on 03/05/2017.
//

#ifndef FEMPLAYGROUND_FPOGREANIMATEDMESHVERTEXPOSITIONSSOURCE_H
#define FEMPLAYGROUND_FPOGREANIMATEDMESHVERTEXPOSITIONSSOURCE_H

#include "IRTFVertexPositionsSource.h"
#include "CLDeduplicatedMesh.h"

class FPOgreAnimatedMeshVertexPositionsSource: public IRTFVertexPositionsSource
{
public:
    FPOgreAnimatedMeshVertexPositionsSource(shared_ptr<CLDeduplicatedMesh> mesh, Ogre::SceneNode* node, const string vertexGroup);

    vector<vec3> getRestWorldPositions() override;
    vector<vec3> getCurrentWorldPositions() override;

private:
    shared_ptr<CLDeduplicatedMesh> mMesh;
    Ogre::SceneNode* mNode;
    const string mVertexGroup;
};

#endif //FEMPLAYGROUND_FPOGREANIMATEDMESHVERTEXPOSITIONSSOURCE_H

//
// Created by Damian Stewart on 03/05/2017.
//

#include "FPOgreRTFDebugDrawerAdapter.h"

#include "OgreDebugDrawer.h"

FPOgreRTFDebugDrawerAdapter::FPOgreRTFDebugDrawerAdapter(OgreDebugDrawer *ogreDrawer)
: mOgreDrawer(ogreDrawer)
{

}

static Ogre::Vector3 convertVector(const vec3 &v)
{
    return Ogre::Vector3(v.x, v.y, v.z);
}

static Ogre::ColourValue convertColor(const RTFDebugDrawer::Color &color)
{
    return Ogre::ColourValue(color.r, color.g, color.b, color.a);
}

void FPOgreRTFDebugDrawerAdapter::drawLine(vec3 p1, vec3 p2, RTFDebugDrawer::Color c)
{
    mOgreDrawer->drawLine(convertVector(p1), convertVector(p2), convertColor(c));
}

void FPOgreRTFDebugDrawerAdapter::drawTriangle(vec3 p1, vec3 p2, vec3 p3, RTFDebugDrawer::Color c)
{
    mOgreDrawer->drawTriangle(convertVector(p1), convertVector(p2), convertVector(p3), convertColor(c));
}


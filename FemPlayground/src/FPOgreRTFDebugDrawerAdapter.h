//
// Created by Damian Stewart on 03/05/2017.
//

#ifndef FEMPLAYGROUND_OGRERTFDEBUGDRAWERADAPTER_H
#define FEMPLAYGROUND_OGRERTFDEBUGDRAWERADAPTER_H

#include "RTFDebugDrawer.h"

class FPOgreRTFDebugDrawerAdapter: public RTFDebugDrawer
{
public:
    FPOgreRTFDebugDrawerAdapter(OgreDebugDrawer *ogreDrawer);
private:
public:
    void drawLine(vec3 p1, vec3 p2, Color c) override;

    void drawTriangle(vec3 p1, vec3 p2, vec3 p3, Color c) override;

private:
    OgreDebugDrawer* mOgreDrawer;

};


#endif //FEMPLAYGROUND_OGRERTFDEBUGDRAWERADAPTER_H

//
//  FPSliderHelpers.h
//  FemPlayground
//
//  Created by Damian Stewart on 22/11/15.
//
//

#ifndef FemPlayground_FPSliderHelpers_h
#define FemPlayground_FPSliderHelpers_h

#include "RTFBaseSpringConstraint.h"
#include "CLInterfaceWindowWrapper.h"
#include "RTFTetraObject.h"
#include "RTFWorldPositionConstraint.h"
#include "RTFWallSpatialLimitConstraint.h"
#include "RTFPlaneSpatialLimitConstraint.h"
#include "RTFSpringConstraint.h"
#include "RTFCollisionConstraint.h"

template <class T>
static void addSpringConstraintSlider(string tabName, string name, shared_ptr<T> constraint, double minK, double maxK)
{
    auto constraintBase = std::dynamic_pointer_cast<RTFBaseSpringConstraint>(constraint);
    CLInterfaceWindowWrapper::addLogarithmicSlider(tabName, name, constraintBase->getParams().springK, minK, maxK, [constraintBase](float value) {
        constraintBase->getParams().springK = value;
    });
}


static void addWorldPositionConstraintSliders(string tabName, string name, shared_ptr<RTFWorldPositionConstraint> constraint)
{
    CLInterfaceWindowWrapper::addLogarithmicSlider(tabName, name + " Kp", constraint->getPDController()->getKp(), 1, 1000, [constraint](float value) {
        constraint->getPDController()->setKp(value);
    });
    CLInterfaceWindowWrapper::addLogarithmicSlider(tabName, name + " Kd", constraint->getPDController()->getKd(), 0.1, 100, [constraint](float value) {
        constraint->getPDController()->setKd(value);
    });
}


static void
addSpringLengthSlider(string tabName, string name, shared_ptr<RTFSpringConstraint> constraint, double minLength, double maxLength)
{
    CLInterfaceWindowWrapper::addLogarithmicSlider(tabName, name, constraint->getRestLength(), minLength, maxLength, [constraint](float value) {
        constraint->setRestLength(value);
    });
}

static void addTetraObjectMaterialSliders(string tabName, string name, shared_ptr<RTFTetraObject> object, float maxE = 1e5)
{
    CLInterfaceWindowWrapper::addLogarithmicSlider(tabName, name + " E", object->getMaterialYoungsModulus(), 0.01f, maxE, [object](float value) {
		object->setMaterialYoungsModulus(value);
	});
    CLInterfaceWindowWrapper::addLogarithmicSlider(tabName, name + " mu", object->getMaterialPoissonsRatio(), 0.01f, 0.4999999, [object](float value) {
		object->setMaterialPoissonsRatio(value);
	});
    
}

static void
addWallSpatialLimitSliders(string tabName, string name, shared_ptr<RTFWallSpatialLimitConstraint> constraint, float maxForce)
{
    CLInterfaceWindowWrapper::addLogarithmicSlider(tabName, name + " f", constraint->getForceFactor(), 0.01, maxForce, [constraint](float value) {
        constraint->setForceFactor(value);
    });
    CLInterfaceWindowWrapper::addLogarithmicSlider(tabName, name + " d", constraint->getDamping(), 0.01, 1e3, [constraint](float value) {
        constraint->setDamping(value);
    });
}

static void
addPlaneSpatialLimitSliders(string tabName, string name, vector<shared_ptr<RTFPlaneSpatialLimitConstraint>> constraints, float maxForce)
{
    CLInterfaceWindowWrapper::addLogarithmicSlider(tabName, name + " f", constraints[0]->getForceFactor(), 0.01, maxForce, [constraints](float value) {
        for (auto constraint: constraints) {
            constraint->setForceFactor(value);
        }
    });
    CLInterfaceWindowWrapper::addLogarithmicSlider(tabName, name + " d", constraints[0]->getDamping(), 0.01, 1e3, [constraints](float value) {
        for (auto constraint: constraints) {
            constraint->setDamping(value);
        }
    });
}

static void addCollisionSliders(string tabName, string name, shared_ptr<RTFCollisionConstraint> constraint, float maxForce)
{
    CLInterfaceWindowWrapper::addLogarithmicSlider(tabName, name + " f", constraint->getForceFactor(), 0.01, maxForce, [constraint](float value) {
        constraint->setForceFactor(value);
    });
    CLInterfaceWindowWrapper::addLogarithmicSlider(tabName, name + " fric", constraint->getFrictionFactor(), 0.01, maxForce, [constraint](float value) {
        constraint->setFrictionFactor(value);
    });
    CLInterfaceWindowWrapper::addCheckbox(tabName, name + " fancy", constraint->getUseFancyBarycentricDetector(), [constraint](bool value) {
        constraint->setUseFancyBarycentricDetector(value);
    });
}


#endif

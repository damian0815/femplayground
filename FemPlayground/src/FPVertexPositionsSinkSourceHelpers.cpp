//
// Created by Damian Stewart on 03/05/2017.
//

#include "CLDeduplicatedMeshFactory.h"
#include "ConfigFile.h"
#include "FPOgreAnimatedMeshVertexPositionsSource.h"
#include "CLAnimatedOgreEntity.h"
#include "RTFClothObject.h"
#include "RTFMultipleObjectUpdater.h"
#include "RTFNewFactory.h"
#include "FPApp.h"
#include <src/CLInterfaceWindowWrapper.h>
#include "FPVertexPositionsSinkSourceHelpers.h"

using std::make_shared;

namespace FPVertexPositionsSinkSourceHelpers
{

    shared_ptr <FPOgreAnimatedMeshVertexPositionsSource>
    buildVertexPositionsSource(Ogre::SceneManager *sceneManager, shared_ptr <CLDeduplicatedMeshManager> deduplicatedMeshManager, const RTFConfigFile &configFile, const string& sectionName)
    {
        auto meshBuildResult = CLDeduplicatedMeshFactory::buildDeduplicatedMesh(sceneManager, deduplicatedMeshManager, configFile, sectionName);
        auto vertexGroup = CLDeduplicatedMeshFactory::getVertexGroupName(configFile, sectionName);
        return make_shared<FPOgreAnimatedMeshVertexPositionsSource>(meshBuildResult.mesh, meshBuildResult.node, vertexGroup);
    }

    shared_ptr <FPOgreAnimatedMeshVertexPositionsSink>
    buildVertexPositionsSink(Ogre::SceneManager *sceneManager, shared_ptr <CLDeduplicatedMeshManager> deduplicatedMeshManager, const RTFConfigFile &configFile,  const string& sectionName)
    {
        auto meshBuildResult =CLDeduplicatedMeshFactory::buildDeduplicatedMesh(sceneManager, deduplicatedMeshManager, configFile, sectionName);
        auto vertexGroup = CLDeduplicatedMeshFactory::getVertexGroupName(configFile, sectionName);
        return make_shared<FPOgreAnimatedMeshVertexPositionsSink>(meshBuildResult.mesh, meshBuildResult.node, vertexGroup);
    }

};

//
// Created by Damian Stewart on 03/05/2017.
//

#ifndef FEMPLAYGROUND_FPVERTEXPOSITIONSSINKSOURCEHELPERS_H
#define FEMPLAYGROUND_FPVERTEXPOSITIONSSINKSOURCEHELPERS_H

#include "FPOgreAnimatedMeshVertexPositionsSource.h"
#include "FPOgreAnimatedMeshVertexPositionsSink.h"
#include "RTFConfigFile.h"

namespace FPVertexPositionsSinkSourceHelpers
{
    shared_ptr <FPOgreAnimatedMeshVertexPositionsSource>
    buildVertexPositionsSource(Ogre::SceneManager *sceneManager, shared_ptr <CLDeduplicatedMeshManager> deduplicatedMeshManager, const RTFConfigFile &configFile, const string& sectionName);

    shared_ptr <FPOgreAnimatedMeshVertexPositionsSink>
    buildVertexPositionsSink(Ogre::SceneManager *sceneManager, shared_ptr <CLDeduplicatedMeshManager> deduplicatedMeshManager, const RTFConfigFile &configFile,  const string& sectionName);
};

#endif //FEMPLAYGROUND_FPVERTEXPOSITIONSSINKSOURCEHELPERS_H

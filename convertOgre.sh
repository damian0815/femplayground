#!/bin/bash

if [ "$#" -ne 1 ]; then
    echo "missing base filename"
    exit 1
fi
meshName=$1

scriptFolder=$(dirname "$0")
# move to script folder
cd "$scriptFolder"

scriptFolderAbsolute=$(pwd)
projectName=$(basename "$scriptFolderAbsolute")

../OgreSDK-1.9.0/bin/macosx/OgreXMLConverter rawData/working/$meshName.mesh.xml
../OgreSDK-1.9.0/bin/macosx/OgreXMLConverter rawData/working/$meshName.skeleton.xml
if [ -f rawData/working/$meshName.smesh ]; then
    tetgen -p rawData/working/$meshName.smesh   
fi

mv rawData/working/$meshName.mesh $projectName/media/models/
mv rawData/working/$meshName.mesh.vertexGroups $projectName/media/models/
mv rawData/working/$meshName.skeleton $projectName/media/models/
mv rawData/working/*.material $projectName/media/materials/scripts/
mv rawData/working/*.png $projectName/media/materials/textures/
mv rawData/working/*.jpg $projectName/media/materials/textures/
mv rawData/working/$meshName.1.ele $projectName/media/physics/
mv rawData/working/$meshName.1.node $projectName/media/physics/
mv rawData/working/$meshName.1.face $projectName/media/physics/
mv rawData/working/$meshName.obj $projectName/media/physics/
mv rawData/working/$meshName.mtl $projectName/media/physics/



